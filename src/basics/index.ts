import {Game} from './core/game'
import {Parser} from './util/parser'
import { User } from './core/user'

let socket = new WebSocket("ws://"+ window.location.hostname +":8082")
let me = new User(true, Math.floor(Math.random() * 100), null)

let game = new Game(socket, me)
export function indexFunc() {
    game.newGame(me.id);

    let parser = new Parser(game)
    socket.onmessage = (e) => {
        parser.parse(e)
    }
}

