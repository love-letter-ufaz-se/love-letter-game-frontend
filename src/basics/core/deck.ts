import { User } from './user'
import { Card } from './card'
import {publish} from '../util/bus'

/** Class that holds deck states. Contains everything going on in Deck */
export class Deck{
    users: Array<User>
    cards: Array<Card>
    me: User
    inGameResponse: string

    /**
     * Create a new Deck
     * @param  {Array<User>} me users in the Deck
     * @param  {Array<Card>} cards cards in the deck (might remove this later)
     */
    constructor(cards: Array<Card>, users: Array<User>) {
        this.me = User.nullUser()
        this.cards = []
        this.cards = cards.slice()
        this.users = []
        for (const user of users) {
            if(user.me) this.me = user
        }
        this.users = users.slice()
        this.inGameResponse = ""
    }

    /**
     * Should play your turn
     * @param card drawn Card
     */
    async my_turn(card: Card): Promise<string>{
        this.me.new_card = card
        let in_hand_resp_1 = this.me?.card?.in_hand(this)
        let in_hand_resp_2 = card.in_hand(this)
        let disc_card = this.prompt_card(...Card.with("enable_for_discard", true, card, this.me?.card))
        let str: string = await disc_card.discard(this)
        return disc_card.num + ":" + str
    }

    end_of_turn(user: User, card: Card, str: string){
        if(user.id !== this.me.id){
            card.discarded_by(this, user, str)
        }
    }
    
    prompt_card(...objs: Array<Card>):Card{
        // TODO Graphics Library should take care of this
        return objs[Math.floor(Math.random() * objs.length)]
    }

    prompt_user(...objs: Array<User>):User{
        // TODO Graphics Library should take care of this
        return objs[Math.floor(Math.random() * objs.length)]
    }

    other_card(card: Card): Card|null{
        if (card === this.me.card){
            return this.me.new_card
        }else if (card === this.me.new_card){
            return this.me.card
        }
        return null
    }

    async card_ingame_backend(str: string): Promise<string>{
        publish('in_game_send', str)
        let responseStr = await this.ingame_backend_wait_promise()
        return responseStr
    }

    ingame_backend_wait = function(str: string) {}

    private ingame_backend_wait_promise(): Promise<string>{
        return new Promise(resolve => {
            this.ingame_backend_wait = function(str: string){
                resolve(str)
            }
        })
    }

    show_users_card(user: User, card: Card){
        // Call Graphics library

        console.log(user.id + "'s card is " + card.num)
    }


}