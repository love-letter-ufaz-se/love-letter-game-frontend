import './deck'
import {Card} from './card'

export class User{
    me: boolean
    id: number
    states: { [key: string]: any}
    card: Card|null
    new_card: Card|null

    /**
     * New User instance
     * @param {boolean} me Tells if it the client user
     * @param {number} id Id of user
     * @param {Card} card initial card of user
     */
    constructor(me: boolean, id: number, card: Card|null) {
        this.me = me
        this.id = id
        this.states = {}
        this.states["knocked_out_of_round"] = false
        this.card = card
        this.new_card = null

    }
    
    hasValidCard(): boolean{
        return this.card == null
    }

    isValid(): boolean{
        return (this.hasValidCard() && this.id > 0)
    }

    static nullUser(): User{
        return new User(false, -1, null)
    }
    static id(id: number, ...users: Array<User>): User{
        users.forEach(user => {
            if(user.id === id) return user
        });
        return User.nullUser()
    }

    static with(key: string, value: any, ...users: Array<User>): Array<User>{
        let ret: Array<User> = []
        users.forEach(user => {
            if(user.states[key] === value) ret.push(user)
        });
        return ret
    }

    static without(key: string, value: any, ...users: Array<User>): Array<User>{
        let ret: Array<User> = []
        users.forEach(user => {
            if(user.states[key] !== value) ret.push(user)
        });
        return ret
    }

}
