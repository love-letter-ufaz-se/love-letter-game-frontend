import {User} from './user'
import {Deck} from './deck'

export abstract class Card{
    num: number
    states: { [key: string]: any}

    constructor(num: number) {
        this.num = num
        this.states = {}
        this.states["enable_for_discard"] = true
    }

    isValid(): boolean{
        return this.num > 0
    }

    /**
     * Filter cards with their states and return filtered
     * @param key key of states object
     * @param value value to be compared with
     * @param cards Cards to be filtered
     */
    static with(key: string, value: any, ...cards: Array<Card|null>): Array<Card>{
        let ret: Array<Card> = []
        cards.forEach(card => {
            if(card != null){
                if(card.states[key] === value) ret.push(card)
            }
        });
        return ret
    }

    static num(num: number, ...cards: Array<Card>): Card|null{
        let ret_card = null
        cards.forEach(card => {
            if(card.num === num){
                ret_card = card
            }
        });

        // return Card.nullCard()
        return ret_card
    }

    

    // abstract static nullCard(): Card

    // Functions to be overwritten by Cards that extend this class

    /**
     * The card is in hand
     */
    abstract in_hand(deck: Deck): string

    /**
     * The card is discarded
     */
    abstract discard(deck: Deck): Promise<string>

    abstract discarded_by(deck: Deck, user: User, str: string): void
}