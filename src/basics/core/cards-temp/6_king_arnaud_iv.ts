import { Card } from "../card"
import { User } from "../user"
import { Game } from '../game'

export class KingArnaud extends Card{

    constructor(){
        super(6)
    }
    in_hand(deck: import("../deck").Deck): string {
        return ""
    }
    async discard(deck: import("../deck").Deck): Promise<string> {
        let users_without_protected = User.without("protected", true, ...deck.users)
        users_without_protected = User.without("knocked_out_of_round", true, ...deck.users)
        if (users_without_protected.length === 0){
            return ""
        }
        let user = deck.prompt_user(...users_without_protected)
        let cardResp = parseInt(await deck.card_ingame_backend(user.id + ":peek"))
        
        let userCard = Card.num(cardResp, ...deck.cards)
        let tmp_card = deck.me.card
        deck.me.card = userCard
        return user.id + ":" + tmp_card?.num
    }
    discarded_by(deck: import("../deck").Deck, user: import("../user").User, str: string): void {
        let user_id_str = str.split(":")

        if(user_id_str.length !== 2)
            throw new Error("King Arnaud: discarded_by() cannot split str")
        
        let user_id = parseInt(user_id_str[0])

        let card_id = parseInt(user_id_str[1])

        if (isNaN(user_id) || isNaN(card_id))
            throw new Error("King Arnaud IV: discarded_by() user_id could not be parsed")

        if (deck.me.id !== user_id || user.id !== deck.me.id)
            return
        
        let userCard = Card.num(card_id, ...deck.cards)

        deck.me.card = userCard

    }

}