import { Card } from "../card";
// import { User } from "../user";

export class HandmaidSusannah extends Card{
    
    constructor(){
        super(4)
    }

    in_hand(deck: import("../deck").Deck): string {
        return "";
    }
    async discard(deck: import("../deck").Deck): Promise<string> {
        deck.me.states["protected"] = true
        return ""
    }
    discarded_by(deck: import("../deck").Deck, user: import("../user").User, str: string): void {        
        user.states["protected"] = true
    }

}