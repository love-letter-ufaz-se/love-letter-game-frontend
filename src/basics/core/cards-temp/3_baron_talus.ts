import { Card } from "../card";
import { User } from "../user";

export class BaronTalus extends Card{
    
    constructor(){
        super(3)
    }

    in_hand(deck: import("../deck").Deck): string {
        return "";
    }
    
    async discard(deck: import("../deck").Deck): Promise<string> {
        let users_without_protected = User.without("protected", true, ...deck.users)
        users_without_protected = User.without("knocked_out_of_round", true, ...deck.users)
        if (users_without_protected.length === 0){
            return ""
        }
        let user = deck.prompt_user(...users_without_protected)
        
        let userCard = parseInt(await deck.card_ingame_backend(user.id + ":peek"))
        if(isNaN(userCard))
            throw new Error("Baron Talus: discard() userCard in NaN")

        if (deck.me.card == null)
            throw new Error("Baron Talus: discard() deck.me.card is null")

        if(deck.me.card.num > userCard){
            deck.card_ingame_backend(user.id + ":knocked_out_of_round")
            return user.id + ""
        }else if (deck.me.card.num < userCard){
            deck.card_ingame_backend(deck.me.id + ":knocked_out_of_round")
            return deck.me.id + ""
        }
        return ""
    }

    discarded_by(deck: import("../deck").Deck, user: import("../user").User, str: string): void {        
        if(str === "")
            return

        let userID = parseInt(str)
        if(isNaN(userID))
            throw new Error("Baron Talus: discarded_by() userID is NaN")

        let knocked_out_of_round_user = User.id(userID, ...deck.users)

        knocked_out_of_round_user.states["knocked_out_of_round"] = true
    }

}