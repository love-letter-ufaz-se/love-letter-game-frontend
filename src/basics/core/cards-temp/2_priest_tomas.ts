import { Card } from "../card";
import { User } from "../user";

export class PriestTomas extends Card{

    constructor(){
        super(2)
    }

    in_hand(deck: import("../deck").Deck): string {
        return ""
    }
    async discard(deck: import("../deck").Deck): Promise<string> {
        let users_without_protected = User.without("protected", true, ...deck.users)
        users_without_protected = User.without("knocked_out_of_round", true, ...deck.users)
        if (users_without_protected.length === 0){
            return ""
        }
        let user = deck.prompt_user(...users_without_protected)

        let userCard = parseInt(await deck.card_ingame_backend(user.id + ":peek"))
        if(isNaN(userCard))
            throw new Error("Priest Tomas: discard() userCard is NaN")

        let theCard = Card.num(userCard, ...deck.cards)
        if(theCard == null)
            throw new Error("Priest Tomas: discard() theCard is null")

        deck.show_users_card(user, theCard)

        return ""
    }
    discarded_by(deck: import("../deck").Deck, user: import("../user").User, str: string): void {
    }

}