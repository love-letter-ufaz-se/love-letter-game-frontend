import { Card } from '../card';

export class PrincessAnnette extends Card{

    constructor (){
        super(8)
    }

    isValid(): boolean {
        return true
    }

    in_hand(deck: import("../deck").Deck): string {
        return ""
    }

    async discard(deck: import("../deck").Deck): Promise<string> {
        deck.me.states["knocked_out_of_round"] = true
        deck.card_ingame_backend(deck.me.id + ":knocked_out_of_round")
        return ""
    }
    
    discarded_by(deck: import("../deck").Deck, user: import("../user").User, str: string): void {
        user.states["knocked_out_of_round"] = true
    }
    
}