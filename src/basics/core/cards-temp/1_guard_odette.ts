import { Card } from "../card";
import { User } from "../user";

export class GuardOdette extends Card{

    constructor(){
        super(1)
    }

    in_hand(deck: import("../deck").Deck): string {
        return ""
    }
    async discard(deck: import("../deck").Deck): Promise<string> {
        let users_without_protected = User.without("protected", true, ...deck.users)
        users_without_protected = User.without("knocked_out_of_round", true, ...deck.users)
        if (users_without_protected.length === 0){
            return ""
        }
        let user = deck.prompt_user(...users_without_protected)

        let card = deck.prompt_card(...deck.cards)

        let userCard = parseInt(await deck.card_ingame_backend(user.id + ":peek"))
        if(isNaN(userCard))
            throw new Error("Guard Odette: discard() userCard is NaN")

        if (userCard === card.num){
            deck.card_ingame_backend(user.id + ":knocked_out_of_round")
            return user.id + ""
        }
        
        return ""
    }
    discarded_by(deck: import("../deck").Deck, user: import("../user").User, str: string): void {
        if(str === "")
            return
        
        let userID = parseInt(str)
        if(isNaN(userID))
            throw new Error("Guard Odette: discarded_by() userID is NaN")

        User.id(userID, ...deck.users).states["knocked_out_of_round"] = true
    }

}