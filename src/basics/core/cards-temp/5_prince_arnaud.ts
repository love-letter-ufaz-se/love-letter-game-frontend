import { Card } from "../card";
import { User } from "../user";

export class PrinceArnaud extends Card{

    constructor(){
        super(5)
    }

    in_hand(deck: import("../deck").Deck): string {
        return ""
    }
    async discard(deck: import("../deck").Deck): Promise<string> {
        let users_without_protected = User.without("protected", true, ...deck.users)
        users_without_protected = User.without("knocked_out_of_round", true, ...deck.users)
        if (users_without_protected.length === 0){
            return ""
        }
        let user = deck.prompt_user(...users_without_protected)
        let userCard = parseInt(await deck.card_ingame_backend(user.id + ":draw_new_card"))
        if(isNaN(userCard))
            throw new Error("Prince Arnaud: discard() userCard is NaN")

        return user.id + ":" + userCard
    }
    discarded_by(deck: import("../deck").Deck, user: import("../user").User, str: string): void {

        // str = <user_id>:<drawed_card>

        let user_id_str = str.split(":")

        if(user_id_str.length !== 2)
            throw new Error("Prince Arnaud: discarded_by() cannot split str")
        
        let user_id = parseInt(user_id_str[0])

        let card_id = parseInt(user_id_str[1])

        if (isNaN(user_id) || isNaN(card_id))
            throw new Error("Prince Arnaud: discarded_by() user_id could not be parsed")

        let card = Card.num(card_id, ...deck.cards)

        if (card == null)
            throw new Error("Prince Arnaud: discarded_by() invalid card given in str")
        
        if (deck.me.id === user_id)
            deck.me.card = card
        
    }

}