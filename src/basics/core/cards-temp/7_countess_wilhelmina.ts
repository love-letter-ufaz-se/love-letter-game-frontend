import { Card } from "../card";

export class CountessWilhelmina extends Card{

    constructor(){
        super(7)
    }

    in_hand(deck: import("../deck").Deck): string {
        let other_card = deck.other_card(this)
        if(other_card == null){
            throw new Error("Countess Wilhelmina: in_hand() cannot find other card")
        }
        if (other_card.num === 6 || other_card.num === 5){
            other_card.states["enable_for_discard"] = false
        }
        return ""

    }

    async discard(deck: import("../deck").Deck): Promise<string> {
        return ""
    }
    
    discarded_by(deck: import("../deck").Deck, user: import("../user").User, str: string): void {
        
    }
    
}