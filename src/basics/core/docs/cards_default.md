# Default Love Game cards

## 8. Princess Annette

#### `discard` => 
```javascript
deck.user.knock_out_of_the_round()
```

## 7. Countess Wilhelmina

#### `in_your_hand` =>
```javascript
if(deck.user.card.num == 6 || deck.user.card.num == 5){
    deck.user.discard(true)
}
```
#### `discard` =>
```javascript
nothing()
```

## 6. King Arnaud IV

#### `discard` =>
```javascript
user = prompt(deck.user, users().without(4))
card = user.card
user.card = deck.user.card
deck.user.card = card
```

## 5. Prince Arnaud

#### `discard` =>
```javascript
user = prompt(deck.user, deck.users.without("protected"))
user.discard(false)
user.draw()
```

## 4. Handmaid Susannah

#### `discard` =>
```javascript
deck.user.tmps["protected"] = true
```

## 3. Baron Talus

#### `discard` =>
```javascript
user = prompt(deck.user, deck.users.without("protected"))
if (user.card.num  < deck.user.card.num){
    user.knock_out_of_the_round()
}else{
    deck.user.knock_out_of_the_round()
}
```

## 2. Priest Tomas

#### `discard` =>
```javascript
user = prompt(deck.user, deck.users)
show(user.card)
```

## 1. Guard Odette

#### `discard` =>
```javascript
card = prompt(deck.user, deck.cards)
user = prompt(deck.user, deck.users.without("protected"))
if (user.card.num == card.num){
    user.knock_out_of_the_round()
}
```