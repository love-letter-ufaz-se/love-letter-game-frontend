import { Deck } from './deck'
import { User } from './user'
import { Card } from './card'
import {subscribe} from './bus'
import { PrincessAnnette } from './cards-temp/8_princess_annette';
import { CountessWilhelmina } from './cards-temp/7_countess_wilhelmina';
import { KingArnaud } from './cards-temp/6_king_arnaud_iv';
import { PrinceArnaud } from './cards-temp/5_prince_arnaud';
import { HandmaidSusannah } from './cards-temp/4_handmaid_susannah';
import { BaronTalus } from './cards-temp/3_baron_talus';
import { PriestTomas } from './cards-temp/2_priest_tomas';
import { GuardOdette } from './cards-temp/1_guard_odette';


// declare namespace GameState {
    
//     export const WAITING = 1
//     export const READY = 2
//     export const UNINITIALIZED = 0
// }

export enum GameState {
    WAITING,
    READY,
    UNINITIALIZED,
}

/*
    TODOs:
        1) Implement round stuff
        2) Extension manager is needed!
*/

/**
 * Game class that regulates the whole game
 */
export class Game{
    user: User
    socket: WebSocket
    id: number
    users: Array<User>
    _turn: number
    deck: Deck | null
    webSocketMSG: Array<string>
    state: number
    
    /**
     * Initialize a new Game object
     * @param {number} extension_id id of the chosen extension
     * @param {WebSocket} socket socket to be used for connection
     * @param {User} user current user
     */
    constructor(socket: WebSocket, user: User) {
        this.socket = socket;
        this.user = user
        this.id = -1
        this.users = []
        this._turn = -1
        subscribe("in_game_send", this.ingame_backend); // add new func name instead of this.func and that is all 
        this.webSocketMSG = [""]
        this.state = GameState.UNINITIALIZED

        // TODO: Add manager to manage extensions and Cards!

        this.deck = null
    }

    /**
     * Request Backend for new Game Set Up
     * @param {number} user_id this user's id
     */
    newGame(user_id: number){
        this.sendMessage(this.user.id + ":start_game")
    }

    /**
     * Game start confirmed by backend. Initialize game configs
     * @param users List of users in *this* Game
     * @param id id of this Game
     */
    startGame(users: Array<User>, id: number, card: Card){
        this.id = id
        this.users = users.slice()
        this.user.card = card

        // TODO: Add manager to manage extensions and Cards!
        this.deck = new Deck(Game.allCards(), this.users)

        this.sendMessage(this.user.id + ":game_ready:" + this.id)
        this._turn = 0
        for (let index = 0; index < users.length; index++) {
            const user = users[index];
            if (user.me){
                break
            }else{
                this._turn++
            }
        }
        
    }

    /**
     * called when backend decleares start of new turn
     */
    new_turn(){
        console.log("New turn. Turn: "+this._turn)
        if(this._turn > 0){
            this._turn--
        }else{
            if(!this.user.states["knocked_out_of_round"]){
                this.sendMessage(this.user.id + ":game_draw_card:" + this.id)
            }
            this._turn = this.users.length - 1
        }
    }

    /**
     * Called when backend gives you card for new turn
     * @param card drawn card
     */
    async my_turn(card: Card){
        let str = await this.deck?.my_turn(card)
        this.sendMessage(this.user.id + ":game_do_card:" + this.id + ":" + str)
    }
    victory(userID: number){
        // Call graphics
        console.log("Winner is: "+userID)
    }
    end_of_turn(user_id: number, card: Card, str: string){
        let user: User = User.id(user_id)
        this.deck?.end_of_turn(user, card, str)
    }

    ingame_backend(e: any){
        this.sendMessage(this.user.id + ":in_game:" + this.id + ":" + e)
    }

    ingame_request_receive_from_backend(str: string): void{
        this.deck?.ingame_backend_wait(str)
    }

    static allCards(): Array<Card>{
        return [new PrincessAnnette(), new CountessWilhelmina(), new KingArnaud(), new PrinceArnaud(), new HandmaidSusannah(), new BaronTalus(), new PriestTomas(), new GuardOdette()]
    }

    private waitForOpenConnection () {
        return new Promise((resolve, reject) => {
            const maxNumberOfAttempts = 10
            const intervalTime = 200 //ms
    
            let currentAttempt = 0
            const interval = setInterval(() => {
                if (currentAttempt > maxNumberOfAttempts - 1) {
                    clearInterval(interval)
                    reject(new Error('Maximum number of attempts exceeded'))
                } else if (this.socket.readyState === this.socket.OPEN) {
                    clearInterval(interval)
                    resolve()
                }
                currentAttempt++
            }, intervalTime)
        })
    }
    
    private async sendMessage (msg: string) {
        if (this.socket.readyState !== this.socket.OPEN) {
            try {
                await this.waitForOpenConnection()
                this.socket.send(msg)
                console.log(msg + " ... sent")
            } catch (err) { console.error(err) }
        } else {
            this.socket.send(msg)
            console.log(msg + " ... sent")
        }
    }
    
}
