/**
 * subscriptions data format:
 * { eventType: { id: callback } }
 */
 
const subscriptions: any = {};
const getNextUniqueId: any = getIdGenerator();

export function subscribe(eventType: any, callback: any) {
  const id = getNextUniqueId();

  if (!subscriptions[eventType]) subscriptions[eventType] = {};

  subscriptions[eventType][id] = callback;

  return {
    unsubscribe: () => {
      delete subscriptions[eventType][id];
      if (Object.keys(subscriptions[eventType]).length === 0)
        delete subscriptions[eventType];
    }
  };
}

export function publish(eventType: any, arg: any) {
  if (!subscriptions[eventType]) return;

  Object.keys(subscriptions[eventType]).forEach(key =>
    subscriptions[eventType][key](arg)
  );
}

function getIdGenerator() {
  let lastId = 0;

  return function getNextUniqueId() {
    lastId += 1;
    return lastId;
  };
}
