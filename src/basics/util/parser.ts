import {Game} from '../core/game'
import {Card} from '../core/card'
import {User} from '../core/user'


enum Command{
    GAME_START,
    NEW_TURN,
    TURN,
    END_OF_TURN,
    VICTORY,
    UNKNOWN_CMD,
}

export class Parser{
    game: Game

    constructor(game: Game) {
        this.game = game
    }
    
    parse(e: MessageEvent){
        console.log(e.data + "... received")
        let cmd = this.parseCommand(e.data)
        switch(cmd){
            case Command.GAME_START:
                this.gameStartCmd(e.data)
                break
            case Command.NEW_TURN:
                this.newTurn(e.data)
                break
            case Command.TURN:
                this.turn(e.data)
                break
            case Command.END_OF_TURN:
                this.end_of_turn(e.data)
                break
            case Command.VICTORY:
                this.victory(e.data)
                break
            
        }
    }

    parseCommand(str: string): Command{
        let strarr = str.split(":")
        let cmd = strarr[0]
        console.log(str)

        switch(cmd){
            case "game_start":
                return Command.GAME_START
            case "new_turn":
                return Command.NEW_TURN
            case "turn":
                return Command.TURN
            case "end_of_turn":
                return Command.END_OF_TURN
            case "victory":
                return Command.VICTORY
            default:
                return Command.UNKNOWN_CMD
        }

    }

    gameStartCmd(str: string){
        // msg = "game_start:<card_id>:<game_id>:...<user_IDs>"
        let strarr = str.split(":")
        let card_id = parseInt(strarr[1])
        if (isNaN(card_id))
            throw new Error("Parser.gameStartCmd: card_id is NaN")
        
        let user_card = Card.num(card_id, ...Game.allCards())
        if(user_card === null)
            throw new Error("Parser.gameStartCmd: user_card is null")

        let userArr: Array<User> = []

        let gameID = parseInt(strarr[2])
        if(isNaN(gameID))
            throw new Error("Parser.gameStartCmd: gameID is NaN")
        
        
        for (let index = 3; index < strarr.length; index++) {
            const userID = parseInt(strarr[index]);
            if(isNaN(userID))
                throw new Error("Parser.gameStartCmd: userID is NaN")
            if(userID === this.game.user.id){
                this.game.user.card = user_card
                userArr.push(this.game.user)
            }else{
                userArr.push(new User(false, userID, null))
            }
        }

        this.game.startGame(userArr, gameID, user_card)
    }

    newTurn(str: string){
        this.game.new_turn()
    }

    turn(str: string){
        // msg = "turn:<user_id>:<card_num>"
        let strarr = str.split(":")
        let userID = parseInt(strarr[1])
        if(isNaN(userID))
            throw new Error("Parser.turn: userID is NaN")
        
        if(this.game.user.id === userID){
            let cardNum = parseInt(strarr[2])
            if(isNaN(cardNum))
                throw new Error("Parser.turn: cardNum is NaN")
            
            let user_card = Card.num(cardNum, ...Game.allCards())
            if(user_card == null)
                throw new Error("Parser.turn: user_card is null")

            this.game.my_turn(user_card)
        }
    }

    end_of_turn(str: string){
        // msg = "end_of_turn:<user_ID>:<card_num>:<additional_str>"
        let strarr = str.split(":")
        let userID = parseInt(strarr[1])
        if(isNaN(userID))
            throw new Error("Parser.end_of_turn: userID is NaN")
        
        let cardNum = parseInt(strarr[2])
        if(isNaN(cardNum))
            throw new Error("Parser.end_of_turn: cardNum is NaN")

        let card = Card.num(cardNum, ...Game.allCards())
        if(card == null)
            throw new Error("Parser.end_of_turn: card is null")
        
        let additional_str = ""
        for (let index = 3; index < strarr.length; index++) {
            const element = strarr[index];
            additional_str = additional_str+element
        }

        this.game.end_of_turn(userID, card, additional_str)
    }
    
    victory(str: string){
        // msg = "victory:<user_ID>"
        let strarr = str.split(":")
        let userID = parseInt(strarr[1])
        if(isNaN(userID))
            throw new Error("Parser.end_of_turn: userID is NaN")
        
        this.game.victory(userID)
    }

}