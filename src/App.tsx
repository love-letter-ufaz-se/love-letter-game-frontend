import React from 'react';

// import classes from "./App.module.css";
import { Main } from "./containers/Main/Main";
import { Wait } from "./components/Wait/Wait";
import { Menu } from "./containers/Menu/Menu";
import { Game } from "./containers/Game/Game";
import {Route, Switch, Redirect } from "react-router-dom";


type props = { };

type AppState = { };

export class App extends React.Component<props, AppState> {
  state: AppState ={ };

  render () {
    return (
        <Switch>
          <Route path="/game" exact component = {Game} />
          <Route path="/menu/:user" exact component = {() => < Menu />} />
          <Route path="/login" exact component = {() => < Main content="login" />} />
          <Route path="/register" exact component = {() => < Main content="register" />} />
          <Route path="/wait" exact component = {() => < Wait actives={3} />} /> 
          <Route exact path="/" component={ () => <Redirect to= "/login" /> } />
        </Switch>
    );
  }
}
