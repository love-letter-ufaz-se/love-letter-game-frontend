import React from "react";
import classes from "./game.module.css";
import {Grass} from "../../assets/iconJSX/Grass/Grass";
// import {Card} from "../../components/Card/Card";
import {Hand} from "../../components/Hand/Hand";
import {Deck} from "../../components/Deck/Deck";
// import {NavItems} from "../../components/NavItems/NavItems";
// import {Logo} from "../../assets/iconJSX/Logo/Logo";
// import {LogIn} from '../LogIn/LogIn';
// import {Route, Switch } from "react-router-dom";


type props = {
  
};

type GameState = {
  
};


export class Game extends React.Component<props, GameState> {
  state: GameState = { };

  render() {
    return (
      <div className={classes.game}>

       <Hand classname="firstHand" />
       <Hand classname="secondHand" />
       <Hand classname="thirdHand" />
       <Hand classname="forthHand" />
       
       <Deck/>
       
       {/*
        
       <Grass className="first" />
       <Grass className="second" />
       <Grass className="third" />
       <Grass className="fourth" />
     */}
      </div>
    );
  }
}

