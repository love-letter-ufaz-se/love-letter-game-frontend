import React from "react";
import classes from "./Main.module.css";
import {Logo} from "../../assets/iconJSX/Logo/Logo";
import {LogIn} from '../LogIn/LogIn';
import {Register} from '../Register/Register';


type props = {
  content: any
};

type MainState = {
  
};


export class Main extends React.Component<props, MainState> {
  state: MainState = {
    
  };

  render() {
    let content = null;

    switch(this.props.content){
      case 'login':
        content = <LogIn />;
        break;
      case 'register':
        content = <Register />;
        break;
      default:
        content = null;
        break;
    }

    
    return (
      <div className={classes.main}>
        <div className={classes.logo}>
          <div className={classes.name}>Love Letter</div>
          <Logo size={60} />
        </div>
          {content}
      </div>
    );
  }
}

