import React from "react";
import classes from "./menu.module.css";
import {NavItems} from "../../components/NavItems/NavItems";
import {Logo} from "../../assets/iconJSX/Logo/Logo";


type props = {
  // user: any
};

type MenuState = {
  nav_list: {icon: string, name: string}[]
};


export class Menu extends React.Component<props, MenuState> {
  state: MenuState = {
    nav_list: [
      { icon: "multiplayer", name: "multiplayer" },
      { icon: "ai", name: "artificial intelligence" },
      { icon: "mods", name: "mods" },
      { icon: "settings", name: "settings" }
    ]
  };

  render() {
    // let user = null;

    // switch(this.props.user){
    //   case 'guest':
    //     user = 'Guest';
    //     break;
    //   case 'userName':
    //     user = 'userName';
    //     break;
    //   default:
    //     user = null;
    //     break;
    // }
    
    let user = window.location.pathname.slice(6);
    let userC = user.charAt(0).toUpperCase() + user.slice(1);

    return (
      <div className={classes.menu}>
      <div className={classes.user}>
        <div className={classes.userPhoto} >{userC.charAt(0)}</div>
        <div className={classes.userName} >{userC}</div>
      </div>
        <div className={classes.logo}>
          <div className={classes.name}>Love Letter</div>
          <Logo size={60} />
        </div>
          <NavItems nav_list={this.state.nav_list} />
      </div>
    );
  }
}

