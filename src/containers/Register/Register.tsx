import React                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        from "react";

import {Button} from "../../components/UI/Button/Button";
import {Spinner} from "../../components/UI/Spinner/Spinner";
import classes from "./register.module.css";
import { NavLink } from "react-router-dom";
// import axios from '../../../axios-orders';
import {Input} from "../../components/UI/Input/Input";

type props = {
};

type RegisterState = {
  orderForm: any
  formIsValid: boolean
  loading: boolean
};


export class Register extends React.Component<props, RegisterState> {
  state: RegisterState = { 
    orderForm: {
      email: {
          elementType: 'input',
          elementConfig: {
            type: 'email',
            placeholder: 'Email Address'
          },
          value: '',
          validation: {
            required: true,
            isEmail: true,
          },
          valid: false,
          touched: false,
        },
      name: {
        elementType: "input",
        elementConfig: {
          type: "text",
          placeholder: "Name"
        },
        value: "",
        validation: {
          required: true
        },
        valid: false,
        touched: false
      },
      password: {
        elementType: "input",
        elementConfig: {
          type: "email",
          placeholder: "Password"
        },
        value: "",
        validation: {
          required: true,
          minLength: true
        },
        valid: false,
        touched: false
      }
    },
    formIsValid: false,
    loading: false
  };

  orderHandler = (event: any) => {
    event.preventDefault();
    this.setState({ loading: true });
    // const formData = {};
    // for (let formElementIdentifier in this.state.orderForm) {
    //     formData[formElementIdentifier] = this.state.orderForm[formElementIdentifier].value;
    // }
    // const order = {
    //     ingredients: this.props.ingredients,
    //     price: this.props.price,
    //     orderData: formData
    // }
    // axios.post( '/orders.json', order )
    //     .then( response => {
    //         this.setState( { loading: false } );
    //         this.props.history.push( '/' );
    //     } )
    //     .catch( error => {
    //         this.setState( { loading: false } );
    //     } );
  };

  checkValidity(value: any, rules: any) {
    let isValid = true;
    if (!rules) {
      return true;
    }

    if (rules.required) {
      isValid = value.trim() !== "" && isValid;
    }

    if (rules.minLength) {
      isValid = value.length >= rules.minLength && isValid;
    }

    if (rules.maxLength) {
      isValid = value.length <= rules.maxLength && isValid;
    }

    if (rules.isEmail) {
      const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
      isValid = pattern.test(value) && isValid;
    }

    if (rules.isNumeric) {
      const pattern = /^\d+$/;
      isValid = pattern.test(value) && isValid;
    }

    return isValid;
  }

  inputChangedHandler = (event: any, inputIdentifier: any) => {
    const updatedOrderForm = {
      ...this.state.orderForm
    };
    const updatedFormElement = {
      ...updatedOrderForm[inputIdentifier]
    };
    updatedFormElement.value = event.target.value;
    updatedFormElement.valid = this.checkValidity(
      updatedFormElement.value,
      updatedFormElement.validation
    );
    updatedFormElement.touched = true;
    updatedOrderForm[inputIdentifier] = updatedFormElement;

    let formIsValid = true;
    for (let inputIdentifier in updatedOrderForm) {
      formIsValid = updatedOrderForm[inputIdentifier].valid && formIsValid;
    }
    this.setState({ orderForm: updatedOrderForm, formIsValid: formIsValid });
  };

  render() {
    const formElementsArray = [];
    for (let key in this.state.orderForm) {
      formElementsArray.push({
        id: key,
        config: this.state.orderForm[key]
      });
    }

    let form = (
      <form onSubmit={this.orderHandler} className={classes.form}>
        {formElementsArray.map(formElement => (
          <Input
            key={formElement.id}
            type={formElement.id}
            elementType={formElement.config.elementType}
            elementConfig={formElement.config.elementConfig}
            value={formElement.config.value}
            invalid={!formElement.config.valid}
            shouldValidate={formElement.config.validation}
            touched={formElement.config.touched}
            changed={(event: any) => this.inputChangedHandler(event, formElement.id)}
          />
        ))}      
        <NavLink to="/login" exact className={classes.link}>LogIn</NavLink> 
        <Button btnType="login" disabled={!this.state.formIsValid}>Sign Up</Button>
      </form>
    );
    if (this.state.loading) {
      form = <Spinner />;
    }
    return <div className={classes.register}>{form}</div>;
  }
}

