import React from "react";
import  './card.css'

interface props {
	type: any
	// classname: any
	id: any
	handName: any
};

export const Card: React.FC<props> = (props) => {
  
  let id = '', className = '';

  switch(props.type){
  	case 'big':
  		className = className.concat('bigCard');
  		break;
  	case 'small':
  		className = className.concat('smallCard');
  		break;
  }

  id = [props.handName, 'card', props.id].join('');

  return(
      <div className={className} id={id}>{props.children}</div>
  );

}


