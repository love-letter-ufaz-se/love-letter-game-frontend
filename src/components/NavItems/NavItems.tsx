import React from "react";
import classes from "./NavItems.module.css";
import {NavItem} from "./NavItem/NavItem";

interface props {
  nav_list: any
}

export const NavItems: React.FC<props> = ({nav_list}) => (
  <div className={classes.components}>
    {nav_list.map((nav_item: any) => (
      <NavItem
        icon={nav_item.icon}
        className={nav_item.name}
        name={nav_item.name}
        key={nav_item.name}
      />
    ))}
  </div>
);


