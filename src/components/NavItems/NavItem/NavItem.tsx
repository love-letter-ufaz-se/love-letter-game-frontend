import React from "react";
import classes from "./NavItem.module.css";
import {AI} from "../../../assets/iconJSX/AI/AI";
import {Mods} from "../../../assets/iconJSX/Mods/Mods";
import {Settings} from "../../../assets/iconJSX/Settings/Settings";
import {MultiPlayer} from "../../../assets/iconJSX/MultiPlayer/MultiPlayer";

interface props{
  icon: any
  className: string
  name: string
}

export const NavItem: React.FC<props> = ({icon, className, name}) => {
  const classNames = [classes.component, className];
  let icon1 = null;
  const iconSize = 100;
  switch (icon) {
    case "ai":
      icon1 = <AI size={iconSize} />;
      break;
    case "mods":
      icon1 = <Mods size={iconSize} />;
      break;
    case "settings":
      icon1 = <Settings size={iconSize} />;
      break;
    case "multiplayer":
      icon1 = <MultiPlayer size={130} />;
      break;
    default:
      icon1 = null;
  }

  return (
    <div className={classNames.join(" ")}>
      <div className={classes.itemicon}>{icon1}</div>
      <span className={classes.name}>{name}</span>
    </div>
  );
};
