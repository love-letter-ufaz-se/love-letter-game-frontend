import React from "react";
import classes from './deck.module.css'
import {Card} from "../../components/Card/Card";

interface props {
  

};

export const Deck: React.FC<props> = (props) => {
  
  return(
      <span className={classes.deck}>
        <Card type="deck" id={1} handName="deck"></Card>
        <Card type="deck" id={2} handName="deck"></Card>
      </span>
  );

}


