import React from "react";
import './hand.css'
import {Card} from "../../components/Card/Card";

interface props {
	
	classname: any

};

export const Hand: React.FC<props> = (props) => {
  
  let className=["hand", props.classname].join(' ');

  return(
      <div className={className}>
	      <div className="smallCardStorage" >
	       	<Card type="small" id={1} handName={props.classname} ></Card>
	        <Card  type="small" id={2} handName={props.classname}></Card> 
	        <Card type="small" id={3} handName={props.classname}></Card>
	      </div>

	      <div className="bigCardStorage">
	      	<Card type="big" id={1} handName={props.classname} ></Card>
	        <Card  type="big" id={2} handName={props.classname}></Card>
	      </div>
      </div>
  );

}


