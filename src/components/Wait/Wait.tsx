import React from "react";

import classes from "./wait.module.css";
import {UserFill} from "../../assets/iconJSX/User/UserFill";
import {UserNoFill} from "../../assets/iconJSX/User/UserNoFill";

interface props{
  actives: any
	
}

export const Wait: React.FC<props> = ({actives}) => {
  let icons = [];

  for(let j=0; j<actives; j++){
      icons.push(<UserFill size={30} key={j} />);
  }

  for(let i=actives; i < 4; i++){ 
    icons.push(<UserNoFill size={30} state="noFill" key={i} />);
  }





  return(
    <div className={classes.waitPage}>
      <div className={classes.wait}>
        <div className={classes.title}>
          <p className={classes.loading}>Waiting for players</p>
        </div>
        <div className={classes.icons}>
          {icons}
        </div>
      </div>
    </div>
  );

}
