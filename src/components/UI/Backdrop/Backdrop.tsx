import React from "react";

import classes from "./Backdrop.module.css";

interface props{
	clicked: (event: React.MouseEvent) => void;
	show: boolean
}

export const Backdrop: React.FC<props> = props =>
  props.show ? (
    <div className={classes.Backdrop} onClick={props.clicked} />
  ) : null;

