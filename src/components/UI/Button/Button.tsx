import React from "react";

import classes from "./Button.module.css";

interface props{
	disabled: boolean
	btnType: string
	// clicked: any
}

export const Button: React.FC<props> = ({disabled, btnType, children}) => (
  <button
    disabled={disabled}
    className={[classes.Button, classes[btnType]].join(" ")}
    // onClick={clicked}
  >
    {children}
  </button>
);
