import React from "react";

import classes from "./Spinner.module.css";



export const Spinner: React.FC = () => <div className={classes.Loader}>Loading...</div>;

