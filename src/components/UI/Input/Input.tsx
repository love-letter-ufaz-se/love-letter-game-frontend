import React from "react";

import classes from "./Input.module.css";

import {Lock} from "../../../assets/iconJSX/Lock/Lock";
import {UserNoFill} from "../../../assets/iconJSX/User/UserNoFill";

interface props{
  invalid: boolean
  shouldValidate: boolean
  touched: boolean
  elementType: any
  changed: any
  elementConfig: {type:string, placeholder: string}
  value: string
  type: string
}

export const Input: React.FC<props> = props => {
  let inputElement = null;
  let icon = null;
  const iconSize = 20;
  const inputClasses = [classes.InputElement];

  if (props.invalid && props.shouldValidate && props.touched) {
    inputClasses.push(classes.Invalid);
  }

  switch (props.elementType) {
    case "input":
      inputElement = (
        <input
          className={inputClasses.join(" ")}
          {...props.elementConfig}
          value={props.value}
          onChange={props.changed}
        />
      );
      break;
    default:
      inputElement = (
        <input
          className={inputClasses.join(" ")}
          {...props.elementConfig}
          value={props.value}
          onChange={props.changed}
        />
      );
  }

  switch (props.type) {
    case "email":
    case "name":
      icon = <UserNoFill size={iconSize} state="login" />;
      break;
    case "password":
      icon = <Lock size={iconSize} />;
      break;
    default:
      icon = null;
  }

  return (
    <div className={classes.Input}>
      {icon}
      {inputElement}
    </div>
  );
};

