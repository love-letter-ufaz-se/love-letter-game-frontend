FROM node:10.19

WORKDIR /app

ENV PATH /app/node_modules/.bin:$PATH

COPY package.json ./
COPY package-lock.json ./
RUN npm install --silent
RUN npm install react-scripts@3.4.1 -g

COPY . ./

EXPOSE 3000

RUN npm install typescript

CMD npm start